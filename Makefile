# Makefile for coordinated generation of plots from data files
# the goal is to generate the (multiple) pdfs, but we trigger this on the single .log file

.PHONY: all
.DELETE_ON_ERROR:

all: $(foreach i, 1 2 3, experiment_bai$(i).log) $(foreach i, 1 2 3, experiment_threshold$(i).log)


experiment_%.log : viz_%.jl experiment_helpers.jl experiment_%.dat
	julia -O3 $< > $@
